<?php

namespace App\Task;

use App\Common\Utils\Log;
use Hyperf\DbConnection\Db;

/**
 * Class ClearLogsTask
 * @package App\Task
 */
class ClearLogsTask
{
    public function execute()
    {
        try {
            go(function (){
                return Db::table('logs')->where('writetime','<=', date('Y-m-d',strtotime('-1 month')))->delete();
            });
        }catch (\Throwable $throwable){
            Log::getInstance()->info('定时任务【清理日志】：失败',['throwable'=>$throwable->getMessage()]);
        }
    }
}
