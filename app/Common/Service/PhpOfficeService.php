<?php

namespace App\Common\Service;

use Closure;
use Hyperf\Di\Annotation\Inject;
use Hyperf\HttpMessage\Stream\SwooleStream;
use Hyperf\HttpServer\Contract\ResponseInterface;
use PhpOffice\PhpSpreadsheet\IOFactory;
use PhpOffice\PhpSpreadsheet\Spreadsheet;
use PhpOffice\PhpSpreadsheet\Style\Fill;

/**
 * Class PhpOfficeService
 * @package App\Common\Service
 */
class PhpOfficeService
{

    /**
     * @Inject
     * @var ResponseInterface
     */
    protected $response;
    /**
     * @param int $pColumnIndex
     * @return mixed|string
     * @author: Zhong Weiwei
     * @Date: 15:13  2022/7/5
     */
    public function stringFromColumnIndex($pColumnIndex = 0)
    {
        $_indexCache = [];
        if (!isset($_indexCache[$pColumnIndex])) {
            if ($pColumnIndex < 26) {
                $_indexCache[$pColumnIndex] = chr(65 + $pColumnIndex);
            } elseif ($pColumnIndex < 702) {
                $_indexCache[$pColumnIndex] = chr(64 + ($pColumnIndex / 26)) .
                    chr(65 + $pColumnIndex % 26);
            } else {
                $_indexCache[$pColumnIndex] = chr(64 + (($pColumnIndex - 26) / 676)) .
                    chr(65 + ((($pColumnIndex - 26) % 676) / 26)) .
                    chr(65 + $pColumnIndex % 26);
            }
        }
        return $_indexCache[$pColumnIndex];
    }

    /**
     * @param array $data
     * @param array $headder
     * @param int $start
     * @param Closure|null $callback
     * @param Closure|null $HeadMerge
     * @throws \PhpOffice\PhpSpreadsheet\Writer\Exception
     * @author: Zhong Weiwei
     * @Date: 15:20  2022/7/5
     */
    public function downloadExcel($data = [], $header = [], int $start = 1, Closure $callback = null)
    {
        set_time_limit(0);
        $head   =   array_values($header);
        $headKeys   =   array_keys($header);
        $spreadsheet = new Spreadsheet();
        $sheet = $spreadsheet->getActiveSheet();
        $count = count($head);
        for ($i = 0; $i < $count; $i++) {
            $str = $this->stringFromColumnIndex($i);

            $sheet->getStyle($str)->applyFromArray([
                'alignment' => [
                    'horizontal' => \PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_CENTER,
                    'vertical' => \PhpOffice\PhpSpreadsheet\Style\Alignment::VERTICAL_CENTER,
                    'wrapText' => true,
                ],
            ]);
            $sheet->getStyle($str)->getAlignment()->setWrapText(true);
            $sheet->getStyle($str . $start)->getFont()->setBold(true);
            $sheet->getStyle($str . $start)->applyFromArray([
                'fill' => [
                    'fillType' => Fill::FILL_SOLID,
                    'startColor' => ['argb' => 'FFC000']
                ]
            ]);
            $sheet->setCellValue($str . $start, $head[$i]);
            $sheet->getColumnDimension($str)->setWidth(20);
        }
        $ak = $this->stringFromColumnIndex($count - 1);
        $callback &&  $callback($sheet,$count);
        $data   =   array_merge([$head],array_map(function ($item) use($headKeys){
            $item   =   (array)$item;
            $itemData   =   [];
            foreach ($headKeys as $key){
                $itemData[$key] =   isset($item[$key]) ? $item[$key] : '';
            }
            return $itemData;
        },$data));
        $sheet->fromArray($data,null,"A{$start}");
        $sheet->getStyle('A'.$start.':' . $ak . count($data))->applyFromArray([
            'borders' => [
                'allBorders' => [
                    'borderStyle' => \PhpOffice\PhpSpreadsheet\Style\Border::BORDER_THIN
                ]
            ],
        ]);
        $writer = IOFactory::createWriter($spreadsheet, 'Xlsx');
        $fileName = md5(time().uniqid().rand(10000,99999)) . '.xlsx';
        $outFilename = BASE_PATH . '/public/' . $fileName;
        $writer->save($outFilename);
        $spreadsheet->disconnectWorksheets();
        unset($spreadsheet);
        return ['path'=>$outFilename,'filename'=>$fileName];
    }

    protected function download(string $content, string $contentType = 'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet')
    {
        return $this->response
            ->withHeader('content-description', 'File Transfer')
            ->withHeader('content-type', $contentType)
//            ->withHeader('content-disposition', "attachment; filename=".rawurlencode($filename))
            ->withHeader('content-transfer-encoding', 'binary')
            ->withHeader('pragma', 'public')
            ->withBody(new SwooleStream($content));
    }

    /**
     * @param array $result
     * @return \Psr\Http\Message\ResponseInterface
     * @author: Zhong Weiwei
     * @Date: 16:22  2022/7/5
     */
    public function saveToBrowserByTmp(string $path, string $contentType = 'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet')
    {
        $content = file_get_contents($path);
        unlink($path);
        return $this->response
            ->withHeader('content-type', $contentType)
//            ->withHeader('content-disposition', "attachment; filename={$result['filename']}")
            ->withHeader('content-transfer-encoding', 'binary')
            ->withHeader('pragma', 'public')
            ->withBody(new SwooleStream($content));
    }
}
