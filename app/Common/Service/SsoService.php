<?php

namespace App\Common\Service;

use App\Common\Traits\ApiResponse;
use App\Common\Utils\Log;
use App\Exception\BusinessException;
use App\Model\Classs;
use App\Model\ClassSpace;
use App\Model\Department;
use App\Model\Grade;
use App\Model\User;
use App\Model\UserDepartment;
use App\Model\UserRole;
use Hyperf\DbConnection\Db;
use Hyperf\Di\Annotation\Inject;
use Hyperf\Guzzle\ClientFactory;

/**
 * Class SsoService
 * @package App\Common\Service
 */
class SsoService
{
    use ApiResponse;
    /**
     * @Inject()
     * @var ClientFactory
     */
    protected $clientFactory;

    protected $unKnowDepartId =  99999;

    public function getAccessToken(string $code): array
    {
        $response = $this->clientFactory->create([
            'base_uri' => config('sso.server_sso_url'),
        ])->post(config('sso.server_sso_url').config('sso.token_url'),[
            'headers'       =>  [
                'Content-Type' =>  'application/x-www-form-urlencoded'
            ],
            'form_params'   =>  [
                'grant_type'    =>  'authorization_code',
                'code'          =>  $code,
                'client_id'     =>  config('sso.oauth_client_id'),
                'client_secret' =>  config('sso.oauth_client_secret'),
                'redirect_uri'  =>  config('sso.oauth_return_url') ?? $this->getDomain().'/accesslogin'
            ]
        ])->getBody()->getContents();

        if (!$response){
            Db::rollBack();
            $this->error('获取令牌失败!');
        }
        $response = json_decode($response, true);
        if (isset($response['error'])){
            Db::rollBack();
            $this->error($response['error']);
        }
        return $response;
    }

    public function getUserInfo(string $access_token): array
    {
        $result =   $this->clientFactory->create([
            'base_uri' => config('sso.server_sso_url'),
        ])->get(config('sso.server_sso_url').config('sso.user_info_url'),[
            'headers'       =>  [
                'Authorization' =>  'Bearer '.$access_token
            ],
        ])->getBody()->getContents();
        Log::info("获取用户详细信息",['result'=>$result]);
        $result =   json_decode($result, true);
        if(!$result || !isset($result['Data']) || !$result['Data']){
            $this->error('获取用户信息失败，请联系管理员');
        }
        return $result['Data'];
    }

    public function getOrganizeInfo(string $organizeid): array
    {
        $result =   $this->clientFactory->create([
            'base_uri' => config('sso.server_sso_url'),
        ])->get(config('sso.server_sso_url').config('sso.organize_info_url'),[
            'query'       =>  [
                'organizeId' =>  $organizeid
            ],
        ])->getBody()->getContents();
        $organizeInfo   =   json_decode($result,true);
        $_data  =   [
            'organizeid'    =>  $organizeInfo['Data']['SingleData']['OrganizeId'],
            'parentid'      =>  $organizeInfo['Data']['SingleData']['ParentId'],
            'organizename'  =>  $organizeInfo['Data']['SingleData']['SchoolName'],
            'shortname'     =>  $organizeInfo['Data']['SingleData']['Description'],
            'organizemail'  =>  $organizeInfo['Data']['SingleData']['Email'],
            'organizecode'  =>  $organizeInfo['Data']['SingleData']['OrganizeCode'],
            'status'        =>  $organizeInfo['Data']['SingleData']['DeleteMark'] == 1 ? 2 : ($organizeInfo['Data']['SingleData']['EnabledMark'] == 1 ? 1 : 0),
        ];
        $check  =   Db::table('organize')->where(['organizeid'=>$_data['organizeid']])->first();
        if ($check){
            Db::table('organize')->where(['organizeid'=>$_data['organizeid']])->update($_data);
        }else{
            Db::table('organize')->insert($_data);
        }
        $this->syncSsoDepartment($organizeid);
        return $_data;
    }

    public function saveUserInfo(array $userInfo)
    {
        $_data  =   [
            'user_name'         =>  $userInfo['RealName'],
            'nickname'          =>  $userInfo['RealName'],
            'login_name'        =>  $userInfo['Account'],
            'mobile'            =>  $userInfo['MobilePhone'],
            'photo'             =>  $userInfo['HeadIcon'],
            'uuid'              =>  $userInfo['Uid'],
            'organizeid'        =>  $userInfo['OrganizeId'],
            'last_login_time'   =>  date('Y-m-d H:i:s'),
            'last_login_ip'     =>  $userInfo['last_login_ip'],
            'user_type'         =>  $userInfo['UserType'],
        ];

        $user   =   User::query()->where('uuid',$_data['uuid'])->first();
        if ($user){
            $_data['id']    =   $user->id;
            $user->update($_data);
            return $user;
        }else{
            $_data['created_at']    =   date('Y-m-d H:i:s');
            $_data['updated_at']    =   date('Y-m-d H:i:s');
            $_data['password']      =   password_hash(config('sso.default_password','xhy@2022'), PASSWORD_DEFAULT);
            $_data['source']        =   'sso';
            $id    =   User::query()->insertGetId($_data);
            return User::query()->where('id',$id)->first();
        }
    }

    /**
     * APP消息推送
     * @param string $organizeid
     * @param array $uids
     * @param string $title
     * @param string $content
     * @throws \GuzzleHttp\Exception\GuzzleException
     * @author: Zhong Weiwei
     * @Date: 16:40  2022/10/8
     */
    public function sendAppMsg(string $organizeid, array $uids, string $title, string $content)
    {
        $sendData   =   [
            'messageType'   =>  1,
            'appID'         =>  config('sso.oauth_client_id'),
            'appName'       =>  config('sso.message.app_name'),
            'orgID'         =>  $organizeid,
            'title'         =>  $title,
            'content'       =>  $content,
            'dataType'      =>  1,
            'PClinkUrl'     =>  config('sso.message.domainpc'),
            'targetIds'     =>  implode(',', $uids),
            'WaplinkUrl'    =>  config('sso.message.domain')
        ];
        $result =   $this->clientFactory->create()->post(config('sso.server_sso_url').config('sso.message.api_notice'),[
            'form_params'   =>  $sendData
        ])->getBody()->getContents();
        $result =   json_decode($result,true);
        if (isset($result['status'])){
            $result['status']   &&  Log::getInstance()->info('APP消息推送成功');
            !$result['status']  &&  Log::getInstance()->error('APP消息推送失败', ['msg' => $result['message'] ?? '推送接口未返回错误信息！']);
        }else{
            Log::getInstance()->error('APP消息推送失败',['error'=>'未知错误，接口未返回错误json！']);
        }
    }

    protected function refreshToken()
    {
        $client = $this->clientFactory->create([
            'base_uri' => config('sso.server_sso_url'),
        ]);
        $response = $client->post(config('sso.token_url'), [
            'headers' => [
                'Content-Type' => 'application/x-www-form-urlencoded'
            ],
            'form_params' => [
                'grant_type' => 'client_credentials',
                'client_id' => config('sso.oauth_client_id'),
                'client_secret' => config('sso.oauth_client_secret'),
                'scope' => config('sso.sync_scope')
            ]
        ])->getBody()->getContents();
        !$response  &&  $this->error('获取令牌失败!');
        $response = json_decode($response, true);
        isset($response['error'])   &&  $this->error($response['error']);
        return $response;
    }

    public function syncSsoDepartment(string $organizeid)
    {
        try {
            $token = $this->RefreshToken();
            $client = $this->clientFactory->create([
                'base_uri' => config('sso.server_sso_url'),
            ]);
            $result = $client->get(config('sso.depart_url'), [
                'query' => [
                    'OrganizeCode' => $organizeid
                ],
                'headers' => [
                    'Authorization' => 'Bearer ' . $token['access_token']
                ],
            ])->getBody()->getContents();
            $deptDataList = json_decode($result, true);
            if (empty($deptDataList)) {
                Log::getInstance()->info('没有数据同步');
            }
            if (isset($deptDataList['Data'])) {
                $departData = $deptDataList['Data'];
                if (!Department::query()->where('organizeid', $organizeid)->where('department_id',$this->unKnowDepartId)->first()){
                    Department::query()->insert([
                        'organizeid'        =>  $organizeid,
                        'department_name'   =>  '未分配',
                        'department_id'     =>  $this->unKnowDepartId,//部门ID
                        'parent_id'         =>  0,// 父部门Id
                        'path'              =>  "/{$this->unKnowDepartId}",
                        'sort'              =>  99,
                        'created_at'        =>  date('Y-m-d H:i:s'),
                        'updated_at'        =>  date('Y-m-d H:i:s'),
                    ]);
                }
                // 添加新获取的
                $list = [];
                foreach ($departData as $value) {
                    $parentId = $value['ParentId'];
                    if ($value['DepartmentFullPath']) {
                        $full = explode('/', $value['DepartmentFullPath']);
                        if (!isset($full[2])) {
                            $parentId = 0;
                        }
                    }
                    $param = [
                        'organizeid'        =>  $value['OrganizeId'],
                        'department_name'   =>  $value['DepartmentName'],//部门名称
                        'department_id'     =>  $value['DepartmentId'],//部门ID
                        'parent_id'         =>  $parentId,// 父部门Id
                        'path'              =>  $value['DepartmentFullPath'],
                        'sort'              =>  isset($value['SortCode']) ? $value['SortCode'] : 0,
                        'created_at'        =>  date('Y-m-d H:i:s'),
                        'updated_at'        =>  date('Y-m-d H:i:s'),
                    ];
                    $list[] = $param;
                }
                //   删除之前存在的
                Department::query()->where('organizeid', $organizeid)->where('department_id', '<>', $this->unKnowDepartId)->delete();
                Department::query()->insert($list);
                $department_ids =   array_unique(array_column($list,'department_id'));
                $uuids  =   UserDepartment::query()->whereNotIn('department_id',$department_ids)->where('organizeid', $organizeid)->distinct()->pluck('uuid')->toArray();
                if ($uuids){
                    UserDepartment::query()->whereIn('uuid',$uuids)->delete();
                    UserDepartment::query()->insert(array_map(function ($uuid) use($organizeid){
                        return  [
                            'uuid'          =>  $uuid,
                            'department_id' =>  $this->unKnowDepartId,
                            'organizeid'    =>  $organizeid,
                            'created_at'    =>  date('Y-m-d H:i:s')
                        ];
                    },$uuids));
                }
                Log::getInstance()->info('同步部门',['msg'=>"同步成功".count($list)."条"]);
            } else {
                Log::getInstance()->info('同步部门', ['msg'=>'没有数据!']);
            }
        } catch (BusinessException $throwable) {
            Log::getInstance()->error('同步部门失败', ['error'=>$throwable->getMessage()]);
        }
    }

    /**
     * 获取接口数据
     * @param string $organizeid
     * @return false|mixed
     * @throws \GuzzleHttp\Exception\GuzzleException
     * @author: Zhong Weiwei
     * @Date: 15:22  2022/8/18
     */
    public function getSsoAllUserList(string $organizeid)
    {
        $client = $this->clientFactory->create([
            'base_uri' => config('sso.server_sso_url'),
        ]);
        $result =   $client->get(config('sso.user_url'),[
            'query'       =>  [
                'orgCode'   =>  $organizeid,
                'userType'=>'teacher|staff|org'
            ],
        ])->getBody()->getContents();
        $result =   json_decode($result,true);
        if (isset($result['StatusCode']) && $result['StatusCode'] == 1){
            return $result['Data']['ListData'];
        }else{
            Log::getInstance()->error('底层获取用户接口未获取到数据',$result);
            return false;
        }
    }

    public function syncSsoUser(string $organizeid)
    {
        try {
            $result   =   $this->getSsoAllUserList($organizeid);
            if ($result){
                Db::beginTransaction();
                $ListData   =   array_map("unserialize",array_unique(array_map("serialize",$result)));
                $uuid   =   array_column($ListData,'F_Uid');
                $currentUuid   =   User::query()->where('organizeid','=', $organizeid)->where('source','sso')->sharedLock()->pluck('uuid')->toArray();
                User::query()->where('organizeid','=', $organizeid)->where('source','sso')->whereNotIn('uuid',$uuid)->delete();
                UserDepartment::query()->where('organizeid', $organizeid)->delete();
                $currentDepartment =   Department::query()->where('organizeid', $organizeid)->pluck('department_id')->toArray();
                $createUserDepartment   =   $parallel   =   $insert =  [];
                foreach ($ListData as $item){
                    $save = [
                        'user_type'     =>  $item['F_UserType'],
                        'user_name'     =>  $item['F_RealName'],
                        'login_name'    =>  $item['F_Account'] ?? $item['F_RealName'],
                        'nickname'      =>  $item['F_RealName'] ?? $item['F_Account'],
                        'mobile'        =>  $item['F_MobilePhone'] ?? '',
                        'photo'         =>  $item['F_HeadIcon'] ??  '',
                        'organizeid'    =>  $item['F_OrganizeId'],
                        'sex'           =>  $item['F_Gender'] == true ? 1 :($item['F_Gender'] == false ? 2 : 3),
                        'uuid'          =>  $item['F_Uid'],
                        'source'        =>  'sso'
                    ];
                    if (in_array($item['F_Uid'],$currentUuid)){
                        $parallel[] =   function () use ($item,$save){
                            return User::query()->where('uuid',$item['F_Uid'])->update($save);
                        };
                    }else{
                        $save['password']   =   password_hash('xhy@2022', PASSWORD_DEFAULT);
                        $save['created_at'] =   date('Y-m-d H:i:s');
                        $save['updated_at'] =   date('Y-m-d H:i:s');
                        $insert[]   =   $save;
                    }
                    $departmentId   =   $item['F_DepartmentId'] ? [$item['F_DepartmentId']] : [$this->unKnowDepartId];
                    $departmentIds  =   (isset($item['Departments']) && $item['Departments'])
                        ? array_merge($departmentId,array_column($item['Departments'],'DepartmentId'))
                        : $departmentId;
                    $unKnowDepartId =   $this->unKnowDepartId;
                    $saveUserDepartment =   array_map(function ($department_id) use ($save,$currentDepartment,$unKnowDepartId){
                        $item   =   [
                            'uuid'          =>  $save['uuid'],
                            'department_id' =>  $department_id,
                            'organizeid'    =>  $save['organizeid'],
                            'created_at'    =>  date('Y-m-d H:i:s')
                        ];
                        if ($currentDepartment && !in_array($department_id,$currentDepartment) || !$department_id){
                            $item['department_id']  =   $unKnowDepartId;
                        }
                        return $item;
                    },$departmentIds);
                    $createUserDepartment   =   array_merge($createUserDepartment,$saveUserDepartment);
                }
                if ($insert){
                    User::query()->insert($insert);
                    Log::getInstance()->info("同步成功【新增】",['msg'=>'新增'.count($insert).'条数据']);
                    $insertUuids    =   array_column($insert,'uuid');
                    $user_ids   =   User::query()->whereIn('uuid',$insertUuids)->pluck('id')->toArray();
                    if ($user_ids){
                        UserRole::query()->whereIn('uid',$user_ids)->where('role_id',3)->delete();
                        UserRole::query()->insert(array_map(function ($uid){
                            return  [
                                'uid'       =>  $uid,
                                'role_id'   =>  3
                            ];
                        },$user_ids));
                    }
                }
                $createUserDepartment   &&  UserDepartment::query()->insert($createUserDepartment);
                Db::commit();
                $parallel &&  parallel($parallel,100) && Log::getInstance()->info("同步成功【修改】",['msg'=>'修改'.count($parallel).'条数据']);
                Log::getInstance()->info("同步用户成功",['msg'=>"同步成功,新增".count($insert)."条更新".count($parallel)."条"]);
            }
        }catch (\Throwable $throwable){
            Db::rollBack();
            Log::getInstance()->error('同步用户失败',['msg'=>$throwable->getMessage()]);
        }
    }

    /**
     * 同步班级
     * @param string $organizeid
     * @author: Zhong Weiwei
     * @Date: 14:15  2022/7/4
     */
    public function syncClass(string $organizeid)
    {
        try {
            $client = $this->clientFactory->create([
                'base_uri' => config('sso.server_sso_url'),
            ]);
            $result =   $client->get(config('sso.class_url'),[
                'query'       =>  [
                    'OrganizeCode'   =>  $organizeid,
                ],
            ])->getBody()->getContents();
            $result =   json_decode($result,true);
            if (isset($result['StatusCode']) && $result['StatusCode'] == 1) {
                if ($result['Data'] && isset($result['Data']['ListData'])){
                    $className  =   Classs::query()->whereIn('shortname',array_column($result['Data']['ListData'],'ShortName'))->where('organizeid',$organizeid)->pluck('shortname')->toArray();
                    $parallel =   $save   =   [];
                    foreach ($result['Data']['ListData'] as $item){
                        $_data = [
                            'code'          =>  $item['DepartmentId'],
                            'name'          =>  $item['DepartmentName'] ?? '',
                            'shortname'     =>  $item['ShortName'] ?? '',
                            'organizeid'    =>  $organizeid,
                            'grade_id'      =>  intval($item['GradeCode']),
                            'year'          =>  intval($item['Year']),
                            'source'        =>  1,
                            'sort'          =>  intval($item['SortCode']),
                            'manager_id'    =>  $item['ManagerId'] ?? '',
                            'term_code'     =>  $item['TermName'],
                            'created_at'    =>  date('Y-m-d H:i:s'),
                            'updated_at'    =>  date('Y-m-d H:i:s'),
                        ];
                        if ($className && in_array($item['ShortName'],$className)){
                            $parallel[] =   function () use ($_data,$organizeid){
                                return Classs::query()->where('shortname',$_data['shortname'])->where('organizeid',$organizeid)->update($_data);
                            };
                        }else{
                            $save[] =   $_data;
                        }

                        if ($item['ManagerId']){
                            $check  =   ClassSpace::query()->where([
                                'uuid'          =>  $item['ManagerId'],
                                'organizeid'    =>  $organizeid,
                                'year'          =>  intval($item['Year']),
                                'class_id'      =>  $item['DepartmentId']
                            ])->count('id');
                            !$check &&  ClassSpace::query()->insert([
                                'name'          =>  $item['ShortName'],
                                'uuid'          =>  $item['ManagerId'],
                                'organizeid'    =>  $organizeid,
                                'year'          =>  intval($item['Year']),
                                'class_id'      =>  $item['DepartmentId'],
                                'created_at'    =>  date('Y-m-d H:i:s'),
                                'updated_at'    =>  date('Y-m-d H:i:s'),
                            ]);
                        }

                    }
                    if ($parallel){
                        parallel($parallel,100);
                        Classs::query()->where('organizeid',$organizeid)->whereNotIn('shortname',$className)->where('source',1)->delete();
                        Log::getInstance()->info('同步班级成功',['msg'=>"同步".count($parallel)."条数据"]);
                    }else{
                        Classs::query()->where('organizeid',$organizeid)->where('source',1)->delete();
                    }
                    $save   &&  Classs::query()->insert($save) &&  Log::getInstance()->info('同步班级成功',['msg'=>"同步".count($save)."条数据"]);
                    !$save  &&  !$parallel   && Log::getInstance()->info('底层接口未获取到班级数据',(array)$result);
                }else{
                    Log::getInstance()->info('同步班级成功',['msg'=>'同步0条数据']);
                }
            }else{
                Log::getInstance()->info('同步班级失败',(array)$result);
            }
        }catch (\Throwable $throwable){
            Log::getInstance()->info('同步班级失败',['msg'=>$throwable->getMessage()]);
        }
    }

    /**
     * 同步年级
     * @param string $organizeid
     * @author: Zhong Weiwei
     * @Date: 14:31  2022/7/4
     */
    public function syncGrade(string $organizeid)
    {
        try {
            $client = $this->clientFactory->create([
                'base_uri' => config('sso.server_sso_url'),
            ]);
            $result = $client->get(config('sso.grade_url'), [
                'query' => [
                    'organizeId' => $organizeid,
                ],
            ])->getBody()->getContents();
            $result = json_decode($result, true);
            if (isset($result['StatusCode']) && $result['StatusCode'] == 1) {
                if ($result['Data'] && isset($result['Data']['ListData'])) {
                    $gradeName  =   Grade::query()->whereIn('name',array_column($result['Data']['ListData'],'Name'))->where('organizeid',$organizeid)->pluck('name')->toArray();
                    $parallel =   $save   =   [];
                    foreach ($result['Data']['ListData'] as $item){
                        $_data = [
                            'code'          =>  $item['Code'],
                            'name'          =>  $item['Name'] ?? '',
                            'organizeid'    =>  $organizeid,
                            'source'        =>  1,
                            'created_at'    =>  date('Y-m-d H:i:s'),
                            'updated_at'    =>  date('Y-m-d H:i:s'),
                        ];
                        if ($gradeName && in_array($item['Name'],$gradeName)){
                            $parallel[] =   function () use ($_data,$organizeid){
                                return Grade::query()->where('name',$_data['name'])->where('organizeid',$organizeid)->update($_data);
                            };
                        }else{
                            $save[] =   $_data;
                        }
                    }
                    if ($parallel){
                        parallel($parallel,100);
                        Grade::query()->where('organizeid',$organizeid)->whereNotIn('name',$gradeName)->where('source',1)->delete();
                        Log::getInstance()->info('同步年级成功',['msg'=>"同步".count($parallel)."条数据"]);
                    }else{
                        Grade::query()->where('organizeid',$organizeid)->where('source',1)->delete();
                    }
                    $save   &&  Grade::query()->insert($save) &&  Log::getInstance()->info('同步年级成功',['msg'=>"同步".count($save)."条数据"]);
                    !$save  &&  !$parallel   && Log::getInstance()->info('底层接口未获取到年级数据',(array)$result);
                } else {
                    Log::getInstance()->info('同步年级成功', ['msg' => '同步0条数据']);
                }
            } else {
                Log::getInstance()->info('同步年级失败', (array)$result);
            }
        } catch (\Throwable $throwable) {
            Log::getInstance()->info('同步年级失败', ['msg' => $throwable->getMessage()]);
        }
    }
}
