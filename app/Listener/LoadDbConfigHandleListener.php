<?php

namespace App\Listener;

use App\Common\Utils\Cache;
use App\Model\Config;
use Hyperf\DbConnection\Db;
use Hyperf\Di\Annotation\Inject;
use Hyperf\Event\Annotation\Listener;
use Hyperf\Event\Contract\ListenerInterface;
use Hyperf\Framework\Event\AfterWorkerStart;

/**
 * @Listener()
 * Class LoadDbConfigHandleListener
 * @package App\Listener
 */
class LoadDbConfigHandleListener implements ListenerInterface
{
    /**
     * @Inject
     * @var \Hyperf\Contract\ConfigInterface
     */
    public $configInterface;

    public function listen(): array
    {
        return [
            AfterWorkerStart::class,
        ];
    }

    public function process(object $event)
    {
        make(Config::class)->loadConfig();
    }
}
