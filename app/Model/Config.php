<?php

declare (strict_types=1);
namespace App\Model;

use App\Common\Utils\Cache;
use Hyperf\DbConnection\Db;
use Hyperf\Di\Annotation\Inject;

/**
 * @property int $id
 * @property string $name
 * @property string $label
 * @property string $type
 * @property int $group_id
 * @property string $data
 * @property string $value
 * @property string $extra
 * @property int $sort
 */
class Config extends Model
{
    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'config';
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [];
    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = ['id' => 'integer', 'group_id' => 'integer', 'sort' => 'integer'];

    /**
     * @Inject
     * @var \Hyperf\Contract\ConfigInterface
     */
    public $configInterface;

    public function loadConfig()
    {
        if (Cache::get('DB_EXTEND_CONFIGS')){
            $configs    =   Cache::get('DB_EXTEND_CONFIGS');
        }else{
            $configs    =   self::query()->get()->toArray();
            Cache::set('DB_EXTEND_CONFIGS',$configs);
        }
        foreach ($configs as $item){
            $item   =   (object)$item;
            if ($item->type == 'line'){
                continue;
            }
            switch ($item->type){
                case 'checkbox':
                    $item->value = json_decode($item->value, true);
                    break;
                case 'singleimg':
                    $item->value = json_decode($item->value, true) ? json_decode($item->value, true)[0]['url'] : '';
                    break;
                case 'multipleimg':
                    $urls   =   [];
                    if (@json_decode($item->value, true)) foreach (json_decode($item->value, true) as $value){
                        $urls[] =   $value['url'];
                    }
                    $item->value    =   $urls;
                    break;
            }
            $item->type   ==  'checkbox'  &&  $item->value = explode(',',$item->value);
            $this->configInterface->set($item->name,$item->value);
        }
    }
}
