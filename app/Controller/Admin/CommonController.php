<?php

namespace App\Controller\Admin;

use App\Common\Service\SsoService;
use App\Common\Utils\Log;
use App\Controller\AbstractController;
use App\Exception\BusinessException;
use App\Model\Role;
use App\Model\UserRole;
use Hyperf\DbConnection\Db;
use Hyperf\Di\Annotation\Inject;
use Hyperf\HttpServer\Annotation\Controller;

/**
 * @Controller()
 * Class CommonContreller
 * @package App\Controller\Admin
 */
class CommonController extends AbstractController
{

    /**
     * @Inject()
     * @var SsoService
     */
    protected $SsoService;


    public function getInitConfig()
    {
        $data   =   [
            'is_sso'    =>  (int)config('sso.is_sso'),
            'url'       =>  config('sso.server_sso_url').'/core/connect/authorize?'.http_build_query([
                    'response_type'     =>  'code',
                    'client_id'         =>  config('sso.oauth_client_id'),
                    'redirect_uri'      =>  config('sso.oauth_return_url',$this->getDomain().'/accesslogin'),
                    'scope'             =>  config('sso.oauth_scope'),
                    'state'             =>  'state'
                ]),
            'siteTitle' =>  config('setup.title')
        ];
        return $this->success($data);
    }

    public function accessLogin()
    {
        Db::beginTransaction();
        try {
            $code   =   $this->request->input('code');
            $response   =   $this->SsoService->getAccessToken($code);
            $userInfo   =   $this->SsoService->getUserInfo($response['access_token']);

            $roles  =   Role::query()->where('status',1)
                ->whereIn('role_identity',$userInfo['UserRoles'])->pluck('id')->toArray();
            if (!$roles){
                Db::rollBack();
                $this->error('未分配权限，请联系管理员!');
            }
            $this->SsoService->getOrganizeInfo($userInfo['OrganizeId']);
            $userInfo['last_login_ip']   =   $this->clientRealIP();
            $user   =   $this->SsoService->saveUserInfo($userInfo);
            $userRole   =   UserRole::query()->whereIn('role_id',$roles)->where('uid',$user->id)->pluck('role_id')->toArray();

            UserRole::query()->where('uid',$user->id)->whereNotIn('role_id',$roles)->delete();
            $roleDiff   =   array_diff($roles,$userRole);
            $roleDiff    &&  UserRole::query()->insert(array_map(function ($role_id) use($user){
                return [
                    'uid'       =>  $user->id,
                    'role_id'   =>  $role_id
                ];
            },$roleDiff));
            Log::info("【用戶：{$user->login_name}】SSO登录成功",['userInfo'=>$user->toArray()]);
            $token  =   $this->auth->login($user);
            Db::commit();
            $this->success($token,'登录成功');
        }catch (BusinessException $throwable){
            Db::rollBack();
            $this->error($throwable->getMessage());
        }
    }
}
