<?php

declare(strict_types=1);
/**
 * This file is part of Hyperf.
 *
 * @link     https://www.hyperf.io
 * @document https://hyperf.wiki
 * @contact  group@hyperf.io
 * @license  https://github.com/hyperf/hyperf/blob/master/LICENSE
 */

use Hyperf\Crontab\Crontab;

return [
    'enable' => true,
    'crontab' => [
        (new Crontab())->setName('ClearLogs')->setRule('0 0 1 * *')->setCallback([App\Task\ClearLogsTask::class, 'execute'])->setMemo('每月1号清理一个月前的日志数据'),
    ],
];
